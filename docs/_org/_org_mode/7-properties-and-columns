-*- mode:org; -*-
#+TITLE: Properties and Columns
* Properties and Columns 
  :RevoltLogBook:
  - State "DONE"       from "TODO"       [2019-11-17 日 11:24]
  :END:
  
  A property is a key-value pair associated with and entry.Properties
  can be set so they are associated with a single entry,with every
  entry in a tree,or with every entry in an Org file.
  
  There are two main applications for properties in Org mode.First
  property  are like tags,but with a value,Image maintaining a file
  where you document bugs and plan releases for a piece of software
  . Instead of using tags like ~release_1~ , ~release_2~, you can use a
  property, say ~Release~, that in different subtrees has different
  values,such as ~1.0~ ,that in different subtrees has different
  values,such as ~1.0~ or ~2.0~. Second, you can use properties to
  implement(very basic) database capabilities [fn:1] in an Org
  buffer.Imagine keeping track of your music CDs,where properties
  could be things such as the album,artist,date of release , members
  of tracks ,and so on.

  Properties can be conveniently edited and viewed in column view (see
  Section 7.5 Column view).
** Property Syntax
   
   Properties are key-value pairs.when they are associated with a
   single entry or with  a tree they need to inserted to a special
   drawer with the name =PROPERTIES= ,which has to be located right
   below a headline,and its planning line when applicable.Each
   property is specified on a single line,with the key - surrounded by
   colon -- first ,and the value after it.

   key are case-insensitive.Here is an example:
   
   [[file:~/org/revolt/source/_org/_org_demo/_customize_property][Case insenstive property]]

  Depending on the value of =org-use-property-inheritance=, a property
  set this way is associated either with a single entry, or with the
  sub-tree defined by the entry.
  
  You may define the allowed values for a particular property ='Xyz'= by
  setting a property ='Xyz_ALL'= This special property is inherited ,so
  if you set it in a level 1 entry,it applies to the entire tree.
  
  when allowed values are defined,setting the corresponding property
  becomes easier and is less prone to typing errors.For the example with
  the CD collection ,  we can pre-define publishers and the number of
  disks in a box like this:

  [[file:~/org/revolt/source/_org/_org_demo/_customize_property][pre-defined properties]]
  
  if you want to set properties that can be inherited by any entry in
  a file,use a line like:
  
  #+BEGIN_EXAMPLE
  .#+PROPERY: NDisks_ALL 1 2 3 4
  #+END_EXAMPLE
  
  if you want to add to the value of an existing property , append a =+=
  to the property name.The following results in the property ='var'=
  having the value ='foo=1 bar =2 '=.
  
  #+BEGIN_EXAMPLE
  ,#+PROPERTY: var foo = 1
  ,#+PROPERTY: var+ bar = 2
  #+END_EXAMPLE
  
  It is also possible to add to the values of inherited properties.The
  following results in the ='Genres'= property having the  ='Classic
  Baroque'= under the ='Goldberg Variations'= subtree.

  [[file:~/org/revolt/source/_org/_org_demo/_customize_property][One Property has more values]]
  
  Note that a property can only have one entry per drawer.
  
  Property values set with the global variable =org-global-properties=
  can be inherited by all entries in all org files.
** Special properties
   Special properties provide an alternative access method to Org mode
   features,like the TODO state or the priority of an entry,discussed
   in the previous chapters.This interface exists so that you can
   include these states in a column view, or to use them in
   queries.The following property names are special and should not be
   used as keys in the  properties drawer:
** Property Searches
   - =C-c / p= 
   
   property searches
** Property Inheritance
   The outline structure of Org documents lends itself to an
   inheritance model of properties: if the parent in a tree has a
   certain property,the children can inherit this property.Org mode
   does not turn this on by default,because it can slow down property
   searches significantly and is often not needed.
   
   However,if you find inheritance useful,you can turn it on by
   setting the variable =org-use-property-inheritance=.It may be set to
   =t= to make all properties inherited from the parents, to a list of
   properties that should be inherited,or to a regular expression that
   matches inherited properties.If a properties has the value =nil=,this
   is interpreted as an explicit un-define of the property,so that
   inheritance search stops at this value and return =nil=.

   Org mode has a few properties fort which inheritance is hard-coded,
   at least for the special applications for which they are used:
   
   - =COULMNS=
   - =CATEGORY=
   - =ARCHIVE=
   - =LOGGING=
** Column View
   A great way to view and edit properties in an outline tree is
   =column view=. In column view,each outline node is turned into a
   table row.Columns in this table provide access to properties of the
   entries. Org mode implements columns by overlaying a tabular
   structure over the headline of each item.While the headlines have
   been turned into a table row,you can still change the visibility of
   the outline tree.For example,you get a compact table by switching
   to "content" view - S-TAB S-TAB, or simply c while column view is
   active -- but you can still open,read,and edit the entry below each
   headline.Or, you can switch to column view after executing a sparse
   tree command and in this way get a table only for the selected
   items. Column view also works in agenda buffers,where queries have
   collected selected items,possibly from a number of files.
*** Defining columns
    Setting up a column view first requires defining the columns,This
    is done by defining a column format line.
*** Scope of column definitions
    To define a column format for an entire file,use a line like:
    
    #+BEGIN_EXAMPLE
    ,#+COLUMN: %25ITEM %TAGS %PRIORITY %TODO
    #+END_EXAMPLE
    
     To specify a format that only applies to a specific tree,add a
     ='column'= property to the top node of that tree,for example:
     
     [[file:~/org/revolt/source/_org/_org_demo/_customize_property][Set a Columns view]]
     
     If a ='COLUMN'= property is present in an entry,it defines columns
     for the entry itself, and for the entire subtree below it. Since
     the column definition is part of the hierarchical structure of
     the document,you can define columns on level 1 that are general
     enough for all sublevel,and more specific columns further
     down,when you edit a deeper part of the tree.
*** column attributes
    A column definition sets the attributes of a column. The general
    definition looks like this:
    
    #+BEGIN_EXAMPLE
    %[WIDTH]PROPERTY[(TITLE)][{SUMMARY-TYPES}]
    #+END_EXAMPLE 
    
    - WIDTH :: An integer specifying the width of the column.If
               omitted,the width is determined automatically.
    - PROPERTY :: The property that should be edited in this
                  column.Special properties representing meta data are
                  allowed here as well.
    - TITLE :: The header text for the column.If omitted,the property
	       named is used.
    - SUMMARY-TYPE :: The Summary type.If specified,the column values
                      for parent nodes are computed from the children.
		      
		      supported summary types are:
		      
                      - ~+~ : Sum numbers in this column.
                      - ~+;%.1f~ : like '+',but format result with '%.1f'.
                      - ~$~ : Currency,short for '+;%.2f'.
                      - ~min~ : Smallest number in column.
                      - ~max~ : Largest number.
                      - ~mean~ : Arithmetic mean of numbers.
                      - ~X~ : Checkbox status, '[X]' if all children are '[X]'
                      - ~X/~ : Checkbox status,'[n/m]'
                      - ~X%~ : Checkbox status, '[n%]'
                      - ~:~ : Sum time, HH:MM ,plain numbers are hours.
                      - ~:min~ : smallest time value in column.
                      - ~:max~ : Largest  time value.
                      - ~:mean~ : Arithmetic mean of time values.
                      - ~@min~ : Minimum age. (in days/hours/mins/seconds).
                      - ~@max~ : Maximum age. (in days/hours/mins/seconds)
                      - =@mean= : Arithmetic mean of ages (in days/hours/mins/seconds)
                      - ~est+~ : add low-high estimates.
 
		      you can also define custom summary  types by setting
		      =org-columns-summary-types=.
	
	
    The ~'est'~ summary type requires further explanation.It is used for
    combining estimates,expressed as log-high ranges.For
    example,instead of estimating a particular task will take 5
    days,you might estimate it as 5-6 days if you're fairly confident
    you know how much work is required, or 1-10 days if you do not
    really know what needs to be done.Both range average at 5.5
    days,but the first represents a more predictable delivery.but the
    first represents a more predictable delivery.
    
    When combing a set of such estimates,simply adding the lows and
    highs produces an unrealistically wide result.Instead, ~'est+'~ adds
    the =statistical mean and variance= of the subtasks,generating a
    final estimate from the sum. for example,suppose you had ten
    tasks,each of which was estimated at 0.5 to 2 days of
    work.Straight addition produces an estimate of 5 to 20
    days,representing what to expect if everything goes either
    extremely well or extremely poorly. In contrast, ='est+'= estimates
    the full job more realistically,at 10-15 days.
    
    Here is a example for a complete columns definition ,along with
    allowed values.
    
    [[file:~/org/revolt/source/_org/_org_demo/_customize_property][One column definition]]

    
  






    
   
* Footnotes

[fn:1] features

