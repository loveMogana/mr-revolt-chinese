.. revolt documentation master file, created by
   sphinx-quickstart on Wed Nov  6 14:35:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to revolt's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   :name: mastertoc
   :glob:
   
   README
   logging
   _read/hacker
   _rst/_org_mode/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
